from django.urls import path

from service_rest.views import api_list_technicians, api_list_appointments, api_delete_finish_appointment, api_list_vins

urlpatterns = [
    path("technicians/", api_list_technicians, name="enter_technician"),
    path("appointments/", api_list_appointments, name="create_appointment"),
    path("appointments/<int:pk>/", api_delete_finish_appointment, name="update_appointment"),
    path("vins/", api_list_vins, name="list_vins"),
]