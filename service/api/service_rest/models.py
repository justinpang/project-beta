from django.db import models
from django.urls import reverse

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(Technician,related_name="appointments", on_delete=models.CASCADE)
    reason = models.TextField()
    finished = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vin} appointment scheduled: {self.date} at {self.time}"
