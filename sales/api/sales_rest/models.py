from django.db import models

# Create your models here.

class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return self.name

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class SalesRecord(models.Model):
    sales_person = SalesPerson
    customer = Customer
    price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return f"{self.sales_person} sold {self.automobile} to {self.customer} for {self.price}"