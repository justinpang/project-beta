from django.contrib import admin

from .models import SalesRecord, AutomobileVO, SalesPerson, Customer

# Register your models here.


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesRecord)
class TechnicianAdmin(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class AppointmentAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass
