from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import SalesRecord, SalesPerson, AutomobileVO, Customer

# Create your views here.


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number", "id"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number"]


class SalesListEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "customer",
        "automobile",
        "price",
    ]

    def get_extra_data(self, o):
        return {
            "vin": o.automobile.vin,
            "sold": o.automobile.sold,
        }

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


@require_http_methods(["GET"])
def automobile_list(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def automobile_delete(request, pk):
    if request.method == "DELETE":
        count, _ = AutomobileVO.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})



@require_http_methods(["GET", "POST"])
def api_list_all_sales(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales,
                encoder=SalesListEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "could not find sale"},
                status=400,
            )
        
@require_http_methods(["GET", "POST"])
def api_list_individual_sales(request):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(sales_person, encoder=SalesPersonEncoder, safe=False)
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:
        if request.method == "DELETE":
            count, _ = SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Customer is unable to be created. Are you sure they're real?"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def customer_delete(request, pk):
    if request.method == "DELETE":
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["GET", "POST"])
def sales_person_list(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(sales_person, encoder=SalesPersonEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Sales person cannot be created. Double check they exist."}
            )
            response.status_code = 400
            return response
