from django.urls import path
from .views import api_list_all_sales, api_list_individual_sales, automobile_list, customer_delete, customer_list, sales_person_list, automobile_delete



urlpatterns = [
    path('sales-persons/', sales_person_list, name="sales_person_list"),
    path('sales-persons/<int:pk>/', api_list_individual_sales, name="api_list_individual_sales"),
    path('customers/', customer_list, name="customer_list"),
    path('customers/<int:pk>/', customer_delete, name="customer_delete"),
    path('sales-records/', api_list_all_sales, name="api_list_all_sales"),
    path('automobileVO/', automobile_list, name="automobile_list"),
    path('automobileVO/<int:pk>/', automobile_delete, name="automobile_delete"),

]