# CarCar

Team:

* Justin Pang - Automobile Service
* Ryan Spurlock - Auto Sales

## Set-up

When initially starting the application, the inventory api must be populated in order to get the appropriate information sent to the services and sales apis. This can be done through creating a superuser and manually entering them through the admin page located at localhost:8100/admin, or using the built in creation forms in the React application. The CarCar app has forms for creating an automobile in inventory(CreateInventory.js), creating a auto manufacturer (CreateManufacturer.js), and to create a vehicle model (CreateVehicleModel.js). All of these forms are located in the navbar dropdown section titled "Inventory". All service and sales features will be found in the navbar under the dropdown for each respective api.

## Design

## Service microservice

Service microservice uses three models: AutomobileVO, Technician, and Appointment.
AutomobileVO is the model within the service microservice that utilizes polling to poll the inventory api for the automobile vin data. Because vins are updated or created in AutomobileVO when it polls the inventory api, any VIN numbers that are in the inventory at one point will be stored within the vin values of the automobileVO. This is important in determining whether a customer appointment is a VIP appointment, and the vin does not have to exist in the inventory api to determine VIP status.

The Technician model is used to add technicians to the service api. Technicians are stored based on their name and employee numbers. This model has a one to many relationship with appointments, where one technician has many appointments with each appointment only having one technician.

The Appointment model is the main model in the service api. It is used to collect vin from AutomobileVO, the customer name, date and time of the appointment, the technician from the technician model, the reason for the appointment, and whether the appointment has been finished or not. The finished variable is used to determine whether an appointment has been completed. Its default value is false, and when the finished appointment button has been pressed, this value becomes true removing it from the appointment list. VIP status is determined when the VIN listed in the appointment model is also present in the list of vins from AutomobileVO. VIP status is marked in the appointment list by yellow starts and coloring of the customer name.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.
