import React from 'react';

class VehicleModelList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          models: [],
        } ;
    }  
    async loadModel() {
        const modelResponse = await fetch("http://localhost:8100/api/models/");
        if(modelResponse.ok){
            const modelData = await modelResponse.json();
            this.setState({models: modelData.models})
        } else {
            console.error('modelData:', modelResponse);
        }
    }
    async componentDidMount() {
        this.loadModel()
    }
    render() {
        return (
            <div>
                <p></p>
                <h2>Vehicle Models</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.models.map(model => {
                        return (
                            <tr key={model.href}>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td><img src={ model.picture_url } height={100} width={100} alt={model.name} ></img></td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
};

export default VehicleModelList;