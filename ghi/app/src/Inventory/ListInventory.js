import React from 'react';

class InventoryList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          automobiles: [],
        } ;
    }  
    async loadAuto() {
        const autoResponse = await fetch("http://localhost:8100/api/automobiles/");
        if(autoResponse.ok){
            const autoData = await autoResponse.json();
            this.setState({automobiles: autoData.autos})
        } else {
            console.error('autoData:', autoResponse);
        }
    }
    async componentDidMount() {
        this.loadAuto()
    }
    render(){
        return (
            <div>
                <p></p>
                <h2>Vehicle models in Inventory</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.automobiles.map(auto => {
                        return (
                            <tr key={auto.href}>
                                <td>{ auto.vin }</td>
                                <td>{ auto.color }</td>
                                <td>{ auto.year }</td>
                                <td>{ auto.model.name }</td>
                                <td>{ auto.model.manufacturer.name }</td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
};

export default InventoryList;