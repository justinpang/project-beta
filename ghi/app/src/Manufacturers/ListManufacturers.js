import React from 'react';

class ManufacturerList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          manufacturers: [],
        } ;
    }  
    async loadManu() {
        const manufacturerResponse = await fetch("http://localhost:8100/api/manufacturers/");
        if(manufacturerResponse.ok){
            const manuData = await manufacturerResponse.json();
            this.setState({manufacturers: manuData.manufacturers})
        } else {
            console.error('manuData:', manufacturerResponse);
        }
    }
    async componentDidMount() {
        this.loadManu()
    }    
    render(){
        return (
            <div>
                <p></p>
                <h2>Manufacturers</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.href}>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}
export default ManufacturerList;