import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './Manufacturers/ListManufacturers';
import CreateManufacturer from './Manufacturers/CreateManufacturer';
import VehicleModelList from './VehicleModels/ListVehicleModels';
import CreateVehicleModel from './VehicleModels/CreateVehicleModel';
import InventoryList from './Inventory/ListInventory';
import CreateInventory from './Inventory/CreateInventory';
import CreateTechnician from './ServiceAppointments/AddTechnician';
import CreateAppointment from './ServiceAppointments/CreateAppointment';
import ListAppointments from './ServiceAppointments/ListAppointments';
import ServiceHistory from './ServiceAppointments/ServiceHistory';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers/list" element={<ManufacturerList />} />
          <Route path="/manufacturers/create" element={<CreateManufacturer />}/>
          <Route path="/models/list" element={<VehicleModelList />} />
          <Route path="/models/create" element={<CreateVehicleModel />}/>
          <Route path="/inventory/list" element={<InventoryList />} />
          <Route path="/inventory/create" element={<CreateInventory />}/>
          <Route path="/technicians/create" element={<CreateTechnician />}/>
          <Route path="/appointments/list" element={<ListAppointments />}/>
          <Route path="/appointments/create" element={<CreateAppointment />}/>
          <Route path="/appointments/history" element={<ServiceHistory />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
